#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "../common/alloc.h"
#include "ll.h"

//Lock stuff here







struct lock_struct {
	pthread_spinlock_t output_spinlock;
};

lock_t *lock_init(int nthreads)
{
	lock_t *lock;

	XMALLOC(lock, 1);
	pthread_spin_init(&lock->output_spinlock, PTHREAD_PROCESS_SHARED);
	return lock;
}

void lock_free(lock_t *lock)
{
	XFREE(lock);
}

void lock_acquire(lock_t *lock)
{
	lock_t *l = lock;
	pthread_spin_lock(&l->output_spinlock);
}

void lock_release(lock_t *lock)
{
	lock_t *l = lock;
	pthread_spin_unlock(&l->output_spinlock);
}



//Nodes and list stuff below








typedef struct ll_node {
	int key;
	struct ll_node *next;
	lock_t *my_lock;
	/* other fields here? */
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	// ll_node_t *tail;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;

	XMALLOC(ret->my_lock, 1);
	pthread_spin_init(&ret->my_lock->output_spinlock, PTHREAD_PROCESS_SHARED);

	/* Other initializations here? */

	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	// ret->tail = ret->head->next;
	// ret->tail->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

int validate(ll_t *ll, ll_node_t *pred, ll_node_t *curr)
{
	ll_node_t *node;

	node = ll->head;
	while (node->key <= pred->key) 
	{
		if (node == pred)
			return (pred->next == curr);
		node = node->next;
	}
	return 0;
}

int ll_contains(ll_t *ll, int key)
{
	ll_node_t *pred, *curr;
	int ret = 0;

	pred = ll->head;
	// curr = pred->next;
	pthread_spin_lock(&pred->my_lock->output_spinlock);
	
	curr = pred->next;
	pthread_spin_lock(&curr->my_lock->output_spinlock);

	while (curr->key < key) {
		pthread_spin_unlock(&pred->my_lock->output_spinlock);
		pred = curr;
		curr = pred->next;
		pthread_spin_lock(&curr->my_lock->output_spinlock);
	}

	pthread_spin_unlock(&pred->my_lock->output_spinlock);
	ret = (key == curr->key);
	pthread_spin_unlock(&curr->my_lock->output_spinlock);

	return ret;	
}

int ll_add(ll_t *ll, int key)
{
	int ret;
	ll_node_t *pred, *curr, *new_node;

	do {
		pred = ll->head;
		curr = pred->next;

		while (curr->key < key) {
			pred = curr;
			curr = curr->next;
		}
		pthread_spin_lock(&pred->my_lock->output_spinlock);
		pthread_spin_lock(&curr->my_lock->output_spinlock);

		if (validate(ll, pred, curr)) {
			if (key != curr->key)
				{
					new_node = ll_node_new(key);
					new_node->next = curr;
					pred->next = new_node;
					ret = 1;
					break;
				}
			else 
				ret = 0;
				break;			
		}
		pthread_spin_unlock(&pred->my_lock->output_spinlock);
		pthread_spin_unlock(&curr->my_lock->output_spinlock);

	} while (1);

	pthread_spin_unlock(&pred->my_lock->output_spinlock);
	pthread_spin_unlock(&curr->my_lock->output_spinlock);

	return ret;
}


int ll_remove(ll_t *ll, int key)
{
	int ret;
	ll_node_t *pred, *curr;

	do {
		pred = ll->head;
		curr = pred->next;

		while (curr->key <= key) {
			if (curr->key == key)
				break;
			pred = curr;
			curr = curr->next;
		}
		pthread_spin_lock(&pred->my_lock->output_spinlock);
		pthread_spin_lock(&curr->my_lock->output_spinlock);

		if (validate(ll, pred, curr)) {
			if (curr->key == key)
				{
					pred->next = curr->next;
					ret = 1;
					break;
				}
			else 
				ret = 0;
				break;			
		}
		pthread_spin_unlock(&pred->my_lock->output_spinlock);
		pthread_spin_unlock(&curr->my_lock->output_spinlock);

	} while (1);

	pthread_spin_unlock(&pred->my_lock->output_spinlock);
	pthread_spin_unlock(&curr->my_lock->output_spinlock);

	return ret;
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
