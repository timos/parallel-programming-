#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "../common/alloc.h"
#include "ll.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
	char padded[64-sizeof(int)-sizeof(struct ll_node *)];
	/* other fields here? */
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
	/* Other initializations here? */
	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;
	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;
	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

#define REF(VAR) ((long) (VAR) & (~1))
#define MARK(VAR) ((long) (VAR) & (1))



ll_node_t *Get_Address(ll_node_t *ptr){
        long long res = (long long)ptr;
        return  (ll_node_t*) REF(res);

}

ll_node_t *get_mark(ll_node_t *ptr,int *ret){
	ll_node_t * ptr1 = Get_Address(ptr->next);
	long long result = (long long)ptr1;
	ll_node_t *ret_value;
	ret_value = (ll_node_t*)REF(result);
	result = (long long)ptr;
	*ret = MARK(result);;
	return ret_value;
}

struct tupla{
	ll_node_t *prev;
	ll_node_t *curr;
};

typedef struct tupla tupla;


tupla* find(ll_node_t *head,int key){
	int marked;
	tupla* tupl;
	ll_node_t *succ,*prev,*curr;
	retry:  while(1){
		prev = head;
		curr = Get_Address(head->next);
		while(1){
			succ = get_mark(curr,&marked);
			while(marked==1){
				if(__sync_bool_compare_and_swap(&prev->next,REF(curr),REF(succ)))
				goto retry;
				curr = succ;
				succ = get_mark(curr,&marked);	
			};
				if(curr->key >= key){
				tupl = (tupla*)malloc(sizeof(tupla));
				tupl->prev = prev;
				tupl->curr = curr;
				return tupl;
			}	
			prev = curr;
			curr = succ;
		};
	};
}

int ll_remove(ll_t *ll, int key)
{
        ll_node_t *prev,*curr,*succ;
        tupla *ret;
        while(1){
                ret = find(ll->head,key);
                prev = ret->prev;
                curr = ret->curr;
		if(curr->key != key)
	        	return 0;
		else{
                        succ = Get_Address(curr->next);
                        int is_deleted = __sync_bool_compare_and_swap(&curr->next,(long)succ|MARK(curr->next),(long) curr->next|1);
                        if(!is_deleted){
        			continue;
			}
                        __sync_bool_compare_and_swap(&prev->next,REF(curr),REF(succ));
                        return 1;
                }
        }
}


int ll_add(ll_t *ll, int key)
{
	ll_node_t *prev,*curr,*succ;
        tupla *ret;
        while(1){
		ret =  find(ll->head,key);
		prev = ret->prev;
		curr = ret ->curr;
                if(curr->key == key)
                        return 0;
                else{
                	ll_node_t * new_node=ll_node_new(key);
                        new_node->next = curr;
			prev->next = (ll_node_t*) REF(prev->next);
                       	if(__sync_bool_compare_and_swap(&prev->next,REF(curr),REF(new_node)))
                       		return 1;
                }
        };

}



int ll_contains(ll_t *ll, int key)
{
	ll_node_t *curr = ll->head;
	ll_node_t *curr1 = curr;
	while(curr->key < key){
		curr = curr->next;
		curr1 = curr;
		curr = (ll_node_t*)REF(curr);
	};
	int num;
	ll_node_t *succ = get_mark(curr,&num);
	return ( (curr->key==key) && (!num)) ;
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
