#include "lock.h"
#include "../common/alloc.h"

#define FALSE 0
#define TRUE  1

// #define FLAG_ENTRY_SIZE 1

typedef struct {
	int tail;
	int flag[64];
	int size;
} array_node_t;

struct lock_struct {
	array_node_t *lock_info;
};

__thread int slot_index;

lock_t *lock_init(int nthreads)
{
	lock_t *lock;
	array_node_t *lock_info;
	int i;

	XMALLOC(lock, 1);
	XMALLOC(lock_info, 1);

	lock->lock_info = lock_info;
	lock->lock_info->size = nthreads;
	lock->lock_info->flag[0] = FALSE;
	lock->lock_info->flag[1] = TRUE;
	for (i = 2; i < nthreads; ++i)
		lock->lock_info->flag[i] = FALSE;
	lock->lock_info->tail = 0;
	// printf("Tail initialized at: %d\n", lock->lock_info->tail);

	return lock;
}

void lock_free(lock_t *lock)
{
	XFREE(lock);
}

void lock_acquire(lock_t *lock)
{
	lock_t *l = lock;

	// slot_index = l->lock_info->tail;
	slot_index = (l->lock_info->tail = ((l->lock_info->tail+1)%(l->lock_info->size)));
	// l->lock_info->tail = (l->lock_info->tail+1)%(l->lock_info->size);
	// printf("Got in at slot: %d and made tail: %d\n", slot_index, l->lock_info->tail);

	while (l->lock_info->flag[slot_index] == FALSE) 
		;
	// printf("Just left from slot: %d and left tail: %d\n", slot_index, l->lock_info->tail);

}

void lock_release(lock_t *lock)
{
	lock_t *l = lock;

	l->lock_info->flag[slot_index] = FALSE;
	l->lock_info->flag[(slot_index+1)%(l->lock_info->size)] = TRUE;
	// printf("Just finished from slot: %d and left tail: %d\n", slot_index, l->lock_info->tail);
}