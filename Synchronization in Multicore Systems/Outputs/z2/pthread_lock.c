#include "lock.h"
#include "../common/alloc.h"
#include <pthread.h>

struct lock_struct {
	pthread_spinlock_t output_spinlock;
};

lock_t *lock_init(int nthreads)
{
	lock_t *lock;

	XMALLOC(lock, 1);
	pthread_spin_init(&lock->output_spinlock, PTHREAD_PROCESS_SHARED);
	return lock;
}

void lock_free(lock_t *lock)
{
	XFREE(lock);
}

void lock_acquire(lock_t *lock)
{
	lock_t *l = lock;
	pthread_spin_lock(&l->output_spinlock);
}

void lock_release(lock_t *lock)
{
	lock_t *l = lock;
	pthread_spin_unlock(&l->output_spinlock);
}
